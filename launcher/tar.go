package launcher

import (
	"archive/tar"
	"io"
	"os"
	"path/filepath"
	"strings"
)

// Tar walks through srcPath and writes each file it founds to the writer.
// dstPath is used as a prefix for all the files of the archive.
func Tar(srcPath, dstPath string, writer io.Writer) error {
	tw := tar.NewWriter(writer)
	defer tw.Close()

	// walk path
	return filepath.Walk(srcPath, func(file string, fi os.FileInfo, err error) error {

		// return on any error
		if err != nil {
			return err
		}

		// return on non-regular files
		if !fi.Mode().IsRegular() {
			return nil
		}

		// create a new dir/file header
		header, err := tar.FileInfoHeader(fi, fi.Name())
		if err != nil {
			return err
		}

		// makes file path relative to srcPath
		header.Name = filepath.Join(dstPath, strings.TrimPrefix(strings.Replace(file, srcPath, "", -1), string(filepath.Separator)))

		// write the header
		if err := tw.WriteHeader(header); err != nil {
			return err
		}

		// open files for taring
		f, err := os.Open(file)
		defer f.Close()
		if err != nil {
			return err
		}

		// copy file data into tar writer
		if _, err := io.Copy(tw, f); err != nil {
			return err
		}

		return nil
	})
}

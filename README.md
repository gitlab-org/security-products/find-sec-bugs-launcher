# find-sec-bugs launcher

GitLab analyzer launcher to integrate the [find-sec-bugs analyzer](https://gitlab.com/gitlab-org/security-products/analyzers/find-sec-bugs) into GitLab [SAST](https://gitlab.com/gitlab-org/security-products/sast).

## How to use

1. Download a binary release or build from sources
1. Run the binary:

    ```sh
    ./find-sec-bugs-launcher analyze /path/to/source/code > ./find-sec-bugs-report.xml
    ```

1. The results will be stored in the `find-sec-bugs-report.xml` file in the current directory.

## Versioning and release process

TODO

# Contributing

If you want to help and extend the list of supported scanners, read the
[contribution guidelines](CONTRIBUTING.md).

FROM scratch
COPY find-sec-bugs-launcher /find-sec-bugs-launcher
ENTRYPOINT ["/find-sec-bugs-launcher"]

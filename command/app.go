package command

import (
	"os"
	"path"

	"github.com/urfave/cli"
)

func App() *cli.App {
	app := cli.NewApp()
	app.Name = path.Base(os.Args[0])
	app.Usage = "GitLab SAST Analyzer Launcher"
	app.Version = "1.0.0"
	app.Author = "GitLab"

	app.Commands = []cli.Command{
		Analyze(),
	}

	return app
}
